# bitbucktとgithubの違い
私はMercurialがわからなくてGitしか使わないので、GithubとBitbucketとで、大きく違う点は特にないなぁと思ってますが、今日時点で気づいた違いをいくつかご紹介します。

!

本当はJIRA連携とかやりたかったんですが、何かうまくいかなかったので諦めました(´・ω・｀)

!

## 自己紹介
* 山口貴之
* @takyam
* http://takl.org/
* bitbucket
  * https://bitbucket.org/takyam/
* github
  * https://github.com/takyam-git
* 株式会社GaiaX

!

## 料金プラン
* bitbucket
  * ユーザー数課金
  * 非公開レポジトリの数も無制限
  * 1レポジトリのユーザー数は無料だと5人まで
  * 企業ユースだとこっちの方が使い勝手よさそう
* github
  * 非公開レポジトリ数課金
  * 1レポジトリのユーザー数は無制限
  * Enterprise版はGithubの機能をそのままに社内のサーバーとかに設置できる
    * でもお高いんでしょ？→お高いです

!

## ソースへのコメント
* bitbucket
  * コミット単位でのコメントはできます
  * [https://bitbucket.org/site/master/issue/3168/comment-on-line-of-commit-bb-2896](https://bitbucket.org/site/master/issue/3168/comment-on-line-of-commit-bb-2896)
  * 一応IssueはあがってるんだけどAtlassianにソースレビュー用の製品が別にあるから政治的に実現しない可能性も・・・？
* github
  * 行単位でのコメントができます
  * 素敵

!

## Issue
* bitbucket
  * 簡単なチケットみたいな感じ
    * ステータス
    * 種類
    * 優先度
    * アサイン
  * あたりが設定できる
  * 細かいチケットワークフローの設定とかはできないっぽい
* github
  * Open/Closeステータスだけあるただの掲示板

!

## wiki
* bitbucket
  * creole記法っていうwiki記法
* github
  * Markdown
  * とかいくつか選べる※

!

## README
* bitbucket
  * Markdown
  * Textile
  * reStructuredText※ 
  * creole記法非対応・・・
  * [https://confluence.atlassian.com/display/BITBUCKET/Displaying+README+Text+on+the+Overview](https://confluence.atlassian.com/display/BITBUCKET/Displaying+README+Text+on+the+Overview)
* github 
  * いろいろ対応
  * [https://github.com/github/markup](https://github.com/github/markup)

!
## その他
* PagesはGithubはレポジトリ単位、Bitbucketはユーザ単位※
* GistはBitbucketに無い


気づいたところは以上です！他にもあれば教えてください！

「※」は発表時に教えていただたいた内容になります